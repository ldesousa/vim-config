" ------- Set up for Vundle -------

set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" add all your plugins here (note older versions of Vundle
" used Bundle instead of Plugin)

"Status Line
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'


"Python Plugins 
Plugin 'tmhedberg/SimpylFold'
Plugin 'vim-scripts/indentpython.vim'
Plugin 'vim-syntastic/syntastic'
Plugin 'nvie/vim-flake8'
Plugin 'ycm-core/YouCompleteMe'

"LaTeX Plugins
Plugin 'LaTeX-Suite-aka-Vim-LaTeX'

"YAML
Plugin 'mrk21/yaml-vim'

"CSV
Plugin 'chrisbra/csv.vim'

"MarkDown
Plugin 'instant-markdown/vim-instant-markdown'

"SQL
Bundle 'vim-scripts/dbext.vim'

" Base16 colour schemes
Plugin 'chriskempson/base16-vim'

" Forgotten colour theme
Plugin 'haystackandroid/forgotten'

" Dark Spectrum theme - supposedly similar to Oblivion
Plugin 'darkspectrum'

" Oxidize colour theme
Plugin 'ZDBioHazard/vim-oxidize'

" Database interaction with vim-dadbod
Plugin 'tpope/vim-dadbod'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" ------- Colour scheme file -------
"  https://pypi.org/project/alacritty-colorscheme/

"if filereadable(expand("~/.vimrc_background"))
"  let base16colorspace=256          " Remove this line if not necessary
"  source ~/.vimrc_background
"endif

" ------- Clipboard -------

:inoremap <C-v> <ESC>"+pa
:vnoremap <C-c> "+y
:vnoremap <C-d> "+d

" Avoid overwriting register with paste
xnoremap p pgvy

" ------- General -------
set tabstop=4
set shiftwidth=4
set expandtab
set clipboard=unnamedplus

" ------- Tab management -------
:vnoremap <C-PageDown> :tabp
:vnoremap <C-PageUp> :tabn

" ------- Saving in INSERT mode -------
inoremap <F2> <C-O>:w<CR>

" ------- Spell checker ------
" Using the spell checker:
" https://www.linux.com/training-tutorials/using-spell-checking-vim/
nnoremap <F3> :set spell spelllang=en_gb<CR>
imap <F3> <C-O><F3>
nnoremap <F6> :set spell spelllang=nl<CR>
imap <F6> <C-O><F6>
nnoremap <F7> :set spell spelllang=pt<CR>
imap <F7> <C-O><F7>
nnoremap <F4> :set nospell<CR>
imap <F4> <C-O><F4>

" ------- Line breaking ---------
nnoremap <F8> gqG
imap <F8> <C-O><F8>
nnoremap <F9> :set textwidth=80<CR>
imap <F9> <C-O><F9>
nnoremap <F10> :set textwidth=0<CR>
imap <F10> <C-O><F10>

" ------- Long line navigation -------
nnoremap <Up> gk
nnoremap <Down> gj
inoremap <Up> <C-O>gk
inoremap <Down> <C-O>gj

" ------- Colour theme --------

colorscheme oxidize

"colorscheme forgotten-dark
"let g:forgotten_dark_CursorLineNr = 'off'
"let g:forgotten_dark_LineNr = 'off' 

" ------- Airline status line --------
set laststatus=2
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

" ------- LaTeX-Suite-aka-Vim-LaTeX Configuration ---------
let g:tex_flavor='latex' "Use vim-latex even when *.tex file is newcreated)
autocmd BufEnter *.tex set sw=2

"To solve the propleme with vim-latexsuite has with ã and â
imap <buffer> <silent> <M-C> <Plug>Tex_MathCal
imap <buffer> <silent> <M-B> <Plug>Tex_MathBF
imap <buffer> <leader>it <Plug>Tex_InsertItemOnThisLine
imap <buffer> <silent> <M-A>  <Plug>Tex_InsertItem
"imap <buffer> <silent> <M-E>  <Plug>Tex_InsertItem
"imap <buffer> <silent> <M-e>  <Plug>Tex_InsertItemOnThisLine
imap <buffer> <silent> \c <Plug>Traditional
map <buffer> <silent> é é
map <buffer> <silent> á á
map <buffer> <silent> ã ã
"imap ã <Plug>Tex_MathCal
"imap é <Plug>Traditional

" -------- YAML --------

au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml foldmethod=indent
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" -------- Python --------

" UTF encoding
set encoding=utf-8
" enable all Python syntax highlighting features
let python_highlight_all=1
syntax on
" PEP 8 guidelines
au BufNewFile,BufRead *.py
    \ set tabstop=4
    \     softtabstop=4
    \     shiftwidth=4
    \     textwidth=79
    \     expandtab
    \     autoindent
    \     fileformat=unix
" show line numbers
set number
" Enable folding
set foldmethod=indent
set foldlevel=99
" Enable folding with the spacebar
nnoremap <space> za
" Flag bad white spaces
"au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

" -------- Turtle --------
au BufRead,BufNewFile *.ttl set filetype=turtle
au BufRead,BufNewFile *.turtle set filetype=turtle

" 
