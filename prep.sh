#!/bin/bash

# Vundle
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim

# Clipboard integration
sudo apt install vim-gui-common

# YouCompleteMe
sudo apt install build-essential cmake python3-dev

