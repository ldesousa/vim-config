1. Run the prep script:
```bash ./prep.sh```

2. Create symbolic link:
```ln -s ~/git/vim-config/.vimrc ~/.vimrc```

3. Start vim and run `:PluginInstall`. There is an error about colour schemes at the start, but it can be ignored.

4. Install YouCompleteMe:
```bash ./YouCompleteMe.sh```
