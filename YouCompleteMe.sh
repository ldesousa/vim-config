#!/bin/bash
# Instrcutions from the manual:
# https://github.com/ycm-core/YouCompleteMe#linux-64-bit
# Before running this run :PluginInstall inside vim

cd ~/.vim/bundle/YouCompleteMe
python3 install.py --clang-completer --rust-completer
